# icanhazhost.name

Random host name generator. Powered by Cloudflare Workers.

## Usage

```bash

$ curl icanhazhost.name
chalette
```

## Word list

Based on the awesome list from [https://www.seriss.com/people/erco/unixtools/hostnames.html](https://)

## Author

Jaakko Leskinen <<jaakko.leskinen@gmail.com>>